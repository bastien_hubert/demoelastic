function DemoController ($scope){

	var defaultQuery = { "min_score": 0, "size": 20, "from": 0, "sort": [ "_score" ], "query": { "dis_max": { "tie_breaker": 0.5, "queries": [ { "bool": { "boost": 5, "should": [ { "query_string": { "default_field": "jdbc.NOMOURAISONSOCIALE", "query": "queryTerm", "default_operator": "AND", "boost": 1 } }, { "fuzzy_like_this_field": { "jdbc.NOMOURAISONSOCIALE.specialFrench": { "boost": 2, "like_text": "queryTerm", "prefix_length": "2" } } } ] } }, { "bool": { "boost": 0.5, "must": [ { "match": { "jdbc.NOMOURAISONSOCIALE.partial": { "boost": 3, "query": "queryTerm", "fuzziness": 0.5, "prefix_length": 2 } } }, { "match": { "jdbc.NOMOURAISONSOCIALE.partial_middle": { "boost": 1, "query": "queryTerm", "fuzziness": 0.5, "prefix_length": 2 } } }, { "match": { "jdbc.NOMOURAISONSOCIALE.partial_back": { "boost": 1, "query": "queryTerm", "fuzziness": 0.5, "prefix_length": 2 } } } ] } } ] } }, "facets": { "statScore": { "statistical": { "script": "doc.score" } } } };
	
	var query;
	var toto;
	if (localStorage['query'] != null){
		query = JSON.parse(localStorage['query']);
	} else {
		query = defaultQuery;
	}


	$scope.assures = new Array();
	$scope.query = JSON.stringify(query, undefined, 2);

	$scope.initQuery = function (){
		$scope.query = JSON.stringify(defaultQuery, undefined, 2);
		query = defaultQuery;
		doRequest($scope.request);
	}

	$scope.saveQuery = function (){
		query = JSON.parse($scope.query);
		$scope.query = JSON.stringify(query, undefined, 2);
		console.log($scope.query);
		localStorage['query'] = $scope.query;
		doRequest($scope.request);
	}

    $scope.$watch('request', function(newValue, oldValue) {
		if (newValue != null){
	        doRequest(newValue);
		}	            
       	
    });
	
	function computeValueMinScore(ecartType){
		return ecartType*2;
	}

    function doRequest(request){
		var ecartType = null;		
		var requete = JSON.stringify(query).replace(/queryTerm/gi, request);
		$.ajax({
			type: 'POST',
			url: 'http://b-hubert:9200/assures2/_search/',
			data : requete,
			dataType: 'json',
			success: function(data) {
				$scope.assures = new Array();
				ecartType = data['facets'].statScore.std_deviation;
				$scope.minScore =  computeValueMinScore(ecartType);
				for (var index in data){						
					for (var index2 in data[index]["hits"]){
						$scope.assures.push(data[index]["hits"][index2]);
						$scope.$apply();
					}
				}
				}
		});
    }
}